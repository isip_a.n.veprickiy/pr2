def calculator():
    while True:
        user = -1
        print("Выберите действие:")
        print("1. сложение")
        print("2. вычитание")
        print("3. умнодение")
        print("4. деление")
        print("5. деление на цело")
        print("6. нахождение остатка от деления")
        print("7. степень")
        print("8. корень числа")
        print("9. синус ")
        print("10. косинус")
        print("11. тангенс")
        print("12. расчет площади и периметра параллелепипеда")
        print("0. вернуться в меню")
        try: 
            user = int(input())
            os.system('CLS')
        except:
            print("Введено неверное значение!")
            delay(1000)
        if user == 0:
            return
        if user > 12:
            print("Введено неверное значение!")
            user = -1
        while user != -1:
            if user != 8 and user != 9 and user != 10 and user != 11 and user != 12:
                print("Введите первое число")
                a = input()
            elif user!=12:
                print("Введите значение")
                a = input()
            if user != 8 and user != 9 and user != 10 and user != 11 and user != 12:
                print("Введите второе число")
                b = input()
            if user == 12:
                print("Введите длину ребра а:")
                a = input()
                print("Введите длину ребра b:")
                b = input()
                print("Введите длину ребра c:")
                c = input()
            try:
                if user == 1:
                    print(f"Answer: {float(a)+float(b)}")
                if user == 2:
                    print(f"Answer: {float(a)-float(b)}")
                if user == 3:
                    print(f"Answer: {float(a)*float(b)}")
                if user == 4:
                    print(f"Answer: {float(a)/float(b)}")
                if user == 5:
                    print(f"Answer: {float(a)//float(b)}")
                if user == 6:
                    print(f"Answer: {float(a)%float(b)}")
                if user == 7:
                    print(f"Answer: {float(a)**float(b)}")
                if user == 8:
                    print(f"Answer: {math.sqrt(float(a))}")
                if user == 9:
                    print(f"Answer: {math.sin(math.radians(float(a)))}")
                if user == 10:
                    print(f"Answer: {math.cos(math.radians(float(a)))}")
                if user == 11:
                    print(f"Answer: {math.tan(math.radians(float(a)))}")
                if user == 12:
                    print(f"P = {Perimetr(float(a),float(b),float(c))}")
                    print(f"S = {Ploshad(float(a),float(b),float(c))}")
                print("хотите продолжить?")
                print("1. да")
                print("2. нет")
                try: 
                    user2 = int(input())
                    if user2 == 2:
                        user = -1
                    elif user2 == 1:
                        os.system('CLS')
                    else:
                        print("Введено неверное значение!")
                        os.system('CLS')
                        user = -1
                except:
                    print("Введено неверное значение!")
                    os.system('CLS')
                    user = -1
            except:
                print("Введены неверные данные")