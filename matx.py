def matrix(columns,rows,start,step):
    for i in range(rows):
        for j in range(columns):
            if i == 0 and j ==0:
                print(str(start).replace('.0', ''), end = " ")
            else: 
                start = start + step
                print(str(start).replace('.0', ''), end = " ")
        print("", end ="\n")
    print("Хоите продолжить?")
    print("1. Да")
    print("2. Нет")
    while True:
        try:
            user = int(input())
        except:
            print("Введено не верное значение")
            delay(2000)
            os.system('CLS')
            return -1
        if user == 1:
            os.system('CLS')
            return 3
        else:
            os.system('CLS')
            return -1