
import os  
import math
from turtle import delay
Perimetr = lambda a, b,c: 4*a+4*b+4*c
Ploshad = lambda a, b,c: 2*(a*b+b*c+a*c)
def calculator():
    while True:
        user = -1
        print("Выберите действие:")
        print("1. сложение")
        print("2. вычитание")
        print("3. умнодение")
        print("4. деление")
        print("5. деление на цело")
        print("6. нахождение остатка от деления")
        print("7. степень")
        print("8. корень числа")
        print("9. синус ")
        print("10. косинус")
        print("11. тангенс")
        print("12. расчет площади и периметра параллелепипеда")
        print("0. вернуться в меню")
        try: 
            user = int(input())
            os.system('CLS')
        except:
            print("Введено неверное значение!")
            delay(1000)
        if user == 0:
            return
        if user > 12:
            print("Введено неверное значение!")
            user = -1
        while user != -1:
            if user != 8 and user != 9 and user != 10 and user != 11 and user != 12:
                print("Введите первое число")
                a = input()
            elif user!=12:
                print("Введите значение")
                a = input()
            if user != 8 and user != 9 and user != 10 and user != 11 and user != 12:
                print("Введите второе число")
                b = input()
            if user == 12:
                print("Введите длину ребра а:")
                a = input()
                print("Введите длину ребра b:")
                b = input()
                print("Введите длину ребра c:")
                c = input()
            try:
                if user == 1:
                    print(f"Answer: {float(a)+float(b)}")
                if user == 2:
                    print(f"Answer: {float(a)-float(b)}")
                if user == 3:
                    print(f"Answer: {float(a)*float(b)}")
                if user == 4:
                    print(f"Answer: {float(a)/float(b)}")
                if user == 5:
                    print(f"Answer: {float(a)//float(b)}")
                if user == 6:
                    print(f"Answer: {float(a)%float(b)}")
                if user == 7:
                    print(f"Answer: {float(a)**float(b)}")
                if user == 8:
                    print(f"Answer: {math.sqrt(float(a))}")
                if user == 9:
                    print(f"Answer: {math.sin(math.radians(float(a)))}")
                if user == 10:
                    print(f"Answer: {math.cos(math.radians(float(a)))}")
                if user == 11:
                    print(f"Answer: {math.tan(math.radians(float(a)))}")
                if user == 12:
                    print(f"P = {Perimetr(float(a),float(b),float(c))}")
                    print(f"S = {Ploshad(float(a),float(b),float(c))}")
                print("хотите продолжить?")
                print("1. да")
                print("2. нет")
                try: 
                    user2 = int(input())
                    if user2 == 2:
                        user = -1
                    elif user2 == 1:
                        os.system('CLS')
                    else:
                        print("Введено неверное значение!")
                        os.system('CLS')
                        user = -1
                except:
                    print("Введено неверное значение!")
                    os.system('CLS')
                    user = -1
            except:
                print("Введены неверные данные")


def stroka():
    user2 = 1
    while user2 == 1:
        print("Данная функция позволяетподсчитать кол-во символов, пробелов и запятых, в введённом пользователем строке")
        print("Введите строку: ")
        user = input()
        print(f"Количество символов: {len(user.replace(' ',''))}")
        print(f"Количество заптых - {user.count(',')}")
        print(f"Количество пробелов - {user.count(' ')}")
        print("хотите продолжить?")
        print("1. да")
        print("2. нет")
        try: 
            user2 = int(input())
            if user2 == 2:
                os.system('CLS')
                return
            elif user2 == 1:
                os.system('CLS')
            else:
                print("Введено неверное значение!")
                os.system('CLS')
                return
        except:
            print("Введено неверное значение!")
            os.system('CLS')
            return

def matrix(columns,rows,start,step):
    for i in range(rows):
        for j in range(columns):
            if i == 0 and j ==0:
                print(str(start).replace('.0', ''), end = " ")
            else: 
                start = start + step
                print(str(start).replace('.0', ''), end = " ")
        print("", end ="\n")
    print("Хоите продолжить?")
    print("1. Да")
    print("2. Нет")
    while True:
        try:
            user = int(input())
        except:
            print("Введено не верное значение")
            delay(2000)
            os.system('CLS')
            return -1
        if user == 1:
            os.system('CLS')
            return 3
        else:
            os.system('CLS')
            return -1





while True:
    print("Выберите функцию:")
    print("1. калькулятор")
    print("2. подсчет в строке")
    print("3. матрица")
    print("0. выход ")
    try: 
        user = int(input())
    except:
        print("Введено неверное значение!")
        os.system('CLS')
    if user == 1:
        os.system('CLS')
        calculator()
    if user == 2:
        os.system("CLS")
        stroka()
    if user == 3:
        while user == 3:
            try:
                print("Введите 1 параметр - это кол-во столбцов в матрице:")
                one = int(input())
                print("Введите 2 параметр - это кол-во строк в матрице:")
                two = int(input())
                print("Введите 3 параметр - это начало, т. е. с какого значения должно начинаться матрица:")
                three = float(input())
                print("Введите 4 параметр - это шаг, т. е. на какое число должно увеличиваться последующее значение матрицы:")
                four = float(input())
                user = matrix(one,two,three,four)
            except:
                print("Некорректный ввод, повторите попытку")
    if user == 0:
        os.system('CLS')
        exit()    


